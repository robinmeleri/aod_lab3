package aod_lab3;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import se.hig.aod.lab3.BSTPriorityQueue;
import se.hig.aod.lab3.DuplicateItemException;
import se.hig.aod.lab3.EmptyQueueException;
import se.hig.aod.lab3.HeapPriorityQueue;

public class Test {

	private static ArrayList<Integer> temp;
	private static ArrayList<Integer> temp6400;

	private static enum SortChoice {
		SORTED, UNSORTED, REVERSE_SORTED
	};

	public static void main(String[] args)
			throws FileNotFoundException, IOException, DuplicateItemException, EmptyQueueException {

		System.out.println("----------------BST SORTED----------------");
		loadBSTQueue(5000, SortChoice.SORTED);
		loadBSTQueue(10000, SortChoice.SORTED);
		loadBSTQueue(20000, SortChoice.SORTED);
		loadBSTQueue(40000, SortChoice.SORTED);
		loadBSTQueue(80000, SortChoice.SORTED);
		loadBSTQueue(160000, SortChoice.SORTED);
		loadBSTQueue(320000, SortChoice.SORTED);
		System.out.println("----------------BST UNSORTED----------------");
		loadBSTQueue(5000, SortChoice.UNSORTED);
		loadBSTQueue(10000, SortChoice.UNSORTED);
		loadBSTQueue(20000, SortChoice.UNSORTED);
		loadBSTQueue(40000, SortChoice.UNSORTED);
		loadBSTQueue(80000, SortChoice.UNSORTED);
		loadBSTQueue(160000, SortChoice.UNSORTED);
		loadBSTQueue(320000, SortChoice.UNSORTED);
		System.out.println("----------------BST REVERSE SORTED----------------");
		loadBSTQueue(5000, SortChoice.REVERSE_SORTED);
		loadBSTQueue(10000, SortChoice.REVERSE_SORTED);
		loadBSTQueue(20000, SortChoice.REVERSE_SORTED);
		loadBSTQueue(40000, SortChoice.REVERSE_SORTED);
		loadBSTQueue(80000, SortChoice.REVERSE_SORTED);
		loadBSTQueue(160000, SortChoice.REVERSE_SORTED);
		loadBSTQueue(320000, SortChoice.REVERSE_SORTED);
		System.out.println("----------------HEAP SORTED-----------------");
		loadHeapQueue(5000, SortChoice.SORTED);
		loadHeapQueue(10000, SortChoice.SORTED);
		loadHeapQueue(20000, SortChoice.SORTED);
		loadHeapQueue(40000, SortChoice.SORTED);
		loadHeapQueue(80000, SortChoice.SORTED);
		loadHeapQueue(160000, SortChoice.SORTED);
		loadHeapQueue(320000, SortChoice.SORTED);
		System.out.println("----------------HEAP UNSORTED-----------------");
		loadHeapQueue(5000, SortChoice.UNSORTED);
		loadHeapQueue(10000, SortChoice.UNSORTED);
		loadHeapQueue(20000, SortChoice.UNSORTED);
		loadHeapQueue(40000, SortChoice.UNSORTED);
		loadHeapQueue(80000, SortChoice.UNSORTED);
		loadHeapQueue(160000, SortChoice.UNSORTED);
		loadHeapQueue(320000, SortChoice.UNSORTED);
		System.out.println("----------------HEAP REVERSE SORTED-----------------");
		loadHeapQueue(5000, SortChoice.REVERSE_SORTED);
		loadHeapQueue(10000, SortChoice.REVERSE_SORTED);
		loadHeapQueue(20000, SortChoice.REVERSE_SORTED);
		loadHeapQueue(40000, SortChoice.REVERSE_SORTED);
		loadHeapQueue(80000, SortChoice.REVERSE_SORTED);
		loadHeapQueue(160000, SortChoice.REVERSE_SORTED);
		loadHeapQueue(320000, SortChoice.REVERSE_SORTED);

	}

	private static ArrayList<Integer> loadList(String path, int size) throws FileNotFoundException, IOException {

		ArrayList<Integer> list = new ArrayList<Integer>();
		int count = 0;

		BufferedReader in = new BufferedReader(new FileReader(path));
		String l;

		while ((l = in.readLine()) != null && count < size) {
			list.add(Integer.parseInt(l));
			count++;
		}

		in.close();
		return list;

	}

	public static void loadHeapQueue(int size, SortChoice sortChoice)

			throws FileNotFoundException, IOException, DuplicateItemException, EmptyQueueException {

		temp = loadList("aod_lab3_data/data_640000.txt", size);
		temp6400 = loadList("aod_lab3_data/data_6400.txt", 6400);
		HeapPriorityQueue<Integer> heap = new HeapPriorityQueue<Integer>();

		switch (sortChoice) {

		case SORTED:
			Collections.sort(temp); 
			for (Integer i : temp) {
				heap.enqueue(i);
			}
			break;
		case UNSORTED:
			for (Integer i : temp) {
				heap.enqueue(i);
			}
			break;

		case REVERSE_SORTED:
			Collections.sort(temp, Collections.reverseOrder());
			for (Integer i : temp) {
				heap.enqueue(i);
			}
			break;

		}

		long t1 = System.currentTimeMillis();
		for (Integer j : temp6400) {
			heap.enqueue(j);
		}
		long enqueueExeTime = System.currentTimeMillis() - t1; 
		System.out.println("Enqueue time for 6400 elements on a sorted HeapQueue of size: " + size + " is: "
				+ enqueueExeTime / 1000.0 + " s");

		long t2 = System.currentTimeMillis();
		for (int i = 0; i < temp6400.size(); i++) {
			heap.dequeue();
		}
		long dequeueExeTime = System.currentTimeMillis() - t2;
		System.out.println("Dequeue time for 6400 elements on a sorted HeapQueue of size: " + size + " is: "
				+ dequeueExeTime / 1000.0 + " s");
		heap.clear();

	}

	public static void loadBSTQueue(int size, SortChoice sortChoice)
			throws FileNotFoundException, IOException, DuplicateItemException, EmptyQueueException {

		temp = loadList("aod_lab3_data/data_640000.txt", size);
		temp6400 = loadList("aod_lab3_data/data_6400.txt", 6400);
		BSTPriorityQueue<Integer> bst = new BSTPriorityQueue<Integer>();

		switch (sortChoice) {

		case SORTED:
			Collections.sort(temp);
			for (Integer i : temp) {
				bst.enqueue(i);
			}

			break;
		case UNSORTED:
			for (Integer i : temp) {
				bst.enqueue(i);
			}

			break;

		case REVERSE_SORTED:
			Collections.sort(temp, Collections.reverseOrder());
			for (Integer i : temp) {
				bst.enqueue(i);
			}

			break;

		}

		long t1 = System.currentTimeMillis();
		for (Integer j : temp6400) {
			bst.enqueue(j);
		}
		long enqueueExeTime = System.currentTimeMillis() - t1;
		System.out.println("Enqueue time for 6400 elements on a " + sortChoice + " BSTQueue of size: " + size + " is: "
				+ enqueueExeTime / 1000.0 + " s");

		long t2 = System.nanoTime();
		for (int i = 0; i < temp6400.size(); i++) {
			bst.dequeue();
		}
		long dequeueExeTime = System.nanoTime() - t2;
		System.out.println("Dequeue time for 6400 elements on a " + sortChoice + " BSTQueue of size: " + size + " is: "
				+ dequeueExeTime / 1000000000.0 + " s");

		bst.clear();
	}

}